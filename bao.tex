\subsection{Executive Summary}

Sound waves propagating in the first 400,000 years after the Big
Bang imprint a characteristic scale in the clustering of matter in
the Universe.  The baryon acoustic oscillations produce a
reasonably sharp peak in the correlation function of galaxies and
other cosmic tracers at a comoving scale of 150 Mpc
\cite{peebles70,sunyaev70,bond84,bond87,jungman96,hu96,hu96a,hu97}.
The length scale of this feature can be accurately predicted from
the simple physics of the early Universe and the measurements of the
CMB anisotropies.  Using this standard ruler, we can measure the 
angular diameter distance and the Hubble parameter as functions of
redshift \cite{tegmark97,goldberg98,efstathiou99,eisenstein98a,eisenstein02,blake03,hu03b,linder03,seo03}.  The method was extensively described in the
recent Weinberg et al. review \cite{Weinberg2012}.

The BAO method has several important advantages.  First, the
simplicity of the physics and the very large physical scale involved
make the method highly robust.  Current theory suggests that the
measurements at $z<3$ can be made to the cosmic variance limit
without being limited by systematic uncertainties.  Second, the method
affords a high level of statistical precision, particularly at
$z>0.5$.  Third, the method allows a direct probe of $H(z)$, further
increasing the leverage at $z>1$.  Fourth, the method allows a 
direct connection to the angular acoustic scale of the CMB, placing
strong constraints on the spatial curvature of the Universe.

The primary challenge of the BAO method is the need for large
redshift surveys.  Surveys to $z=2$ aimed at extracting most of the
BAO information require of order 50 million galaxies.  At $z>2$,
it is likely that Lyman-$\alpha$ forest methods are more advantageous
\textcolor{red}{(e.g. \cite{mcdonald07})}.
The BOSS experiment is presently measuring the BAO from 1/4 of the
sky at $z<0.7$, as well as conducting first measurements from the
Lyman-$\alpha$ forest at $z>2$.  Surveys in the coming decade
\textcolor{red}{
will extend our view at $z>0.7$} by over an order
of magnitude in cosmic volume.

\subsection{Context}

The acoustic peaks were predicted over 40 years ago but only first
detected in the CMB in 1999-2000.  The first detections in lower redshift
galaxy data took another 5 years \cite{cole05,eisenstein05}.  The large scale of the acoustic peak
\textcolor{red}{means that enormous cosmic volumes are required to detect the signal;
only recent generations} of surveys have reached the requisite
volume.  However, the signal has now been detected by several different
groups in six distinct
spectroscopic data sets \cite{cole05,eisenstein05,tegmark06,percival07,
beutler11,blake11c,Anderson2012}
as well as in two photometric redshift data
sets \cite{padmanabhan07,blake07,hutsi10,crocce11,sawangwit11,seo12}.  
The detection in the SDSS-III BOSS Data Release 9 analysis
\cite{Anderson2012} is itself 5$\sigma$; \textcolor{red}{when combined with lower-redshift
SDSS-II data, this reaches 6.5$\sigma$}.  The detection of the
acoustic peaks in the CMB anisotopy data is exquisite.  At this point,
there is no question of the existence of the acoustic peak at low
redshift, only the need to improve the measurement of its scale.

The best current BAO data set is that of the SDSS-III BOSS, which has
published measurements of a 1.7\% distance to $z=0.57$ \cite{Anderson2012}.  Improvements
to about 1\% precision are imminent.  At lower redshift, SDSS-II 
produced a 1.9\% distance measurement to $z=0.35$ \cite{padmanabhan12,xu12a}; this measurement
will soon be improved by BOSS due to higher galaxy sampling density and 
somewhat more sky area.  Furthermore, the 6dF Galaxy Survey produced
a 4.5\% measurement at $z=0.1$ \cite{beutler11}; this will improve only slightly in the
future.  At higher redshift, the WiggleZ survey measured the acoustic
peak in a sampling reaching $z=1$, but the aggregate precision is about
4\% at $z=0.6$ \cite{blake11c}, now superceded by BOSS.  

BOSS is also establishing a new view of the BAO using the clustering
of the intergalactic medium at $2<z<3$ as revealed by the Lyman-$\alpha$
forest \cite{white03,mcdonald07}.  
The forest refers to the fluctuating scattering of light
from distant quasars by the neutral hydrogen absorption in the 121.6~nm
transition from the ground to first excited state.  Each quasar provides
a (noisy) map of the density of the intergalactic medium between us
and the quasar.  Using many quasars, BOSS can infer a 3-dimensional 
map of the IGM and study the large-scale clustering within the map.  This
has yielded a first detection of the BAO at $z>1$ and a 3\% measurement
of the Hubble parameter at $z=2.3$ \cite{busca12,slosar13}.


Looking to the coming decade, the BAO method will continue to provide
a precise and accurate measurement of the cosmic distance scale.  

First, regarding precision, the BAO method requires surveys of very
large cosmic volumes with sufficient sampling to detect fluctuations
at wavenumbers of order $0.2h$~Mpc$^{-1}$.
The sampling requirement is modest, typically of order
$10^{-4}$--$10^{-3}h^3$ galaxies per Mpc$^3$, which is well below
the density of galaxies such as the Milky Way.  This means that one
can choose galaxies that are more observationally convenient, e.g.,
those that are brighter, have stronger spectral features, are easier
to pre-select, etc.

However, the volume requirement is ambitious.  Reaching below 1\% precision
requires surveys of a fair fraction of the sky.  Fortunately, 
multiplexed spectrographs on dedicated telescopes make this possible.
Moreover, because there is
a particular size of the observable Universe at a given redshift, there
is a maximum amount of information that can be learned from the BAO
method at a given redshift.  \textcolor{red}{This limitation is called cosmic variance.}

Figure~\ref{fig:baoforecast} \textcolor{red}{shows the forecasts for the 
available precision from the measurement of the acoustic peak.
This is assuming realistic sampling and performance from 
reconstruction (to be explained below), with the Fisher matrix 
forecasts from \cite{SeoEisenstein2007} and logarithmic bins in $1+z$.
One can see that precisions better than 0.2\% in the angular
diameter distance and 0.4\% in the Hubble parameter are available
at $z>1$}.  Precisions at $z<1$ degrade because of the smaller cosmic
volumes, but are still better than 1\% at $z>0.3$.  Uncertainties will increase
as the inverse square root of the fraction of the sky covered by the survey.

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.6\hsize]{H_logz_4pi.pdf}
\caption{
\textcolor{red}{The available precision, reported as the fractional uncertainty on $D_A(z)$
and H(z).} This precision assumes a measurement of the BAO for a full-sky survey
with realistic sampling and reconstruction performance, based on Fisher
matrix forecasts from \cite{SeoEisenstein2007}.
\textcolor{red}{The horizontal bars represent the size of each bin for which the fractional
uncertainty is reported.}  Uncertainties will increase
as the inverse square root of the fraction of the sky covered by a survey.
One can see that precisions better than 0.2\% in the angular
diameter distance and 0.4\% in the Hubble parameter are available
at $z>1$.  Precisions at $z<1$ degrade because of the smaller cosmic
volumes, but are still better than 1\% at $z>0.3$.  
From \cite{Weinberg2012}; see paper for more explanation.
}
\label{fig:baoforecast}
\end{center}
\end{figure}


It is commonly assumed that because dark energy is subdominant to
matter at $z>1$, there are diminishing returns in studying it at earlier
epochs.  This is not necessarily true, simply because the precision
of the measurements available to cosmic structure surveys increases 
strongly with redshift.
Figure~\ref{fig:density} explores this in a simplified manner, taking the uncertainties
only from $H(z)$.  Measurements of $H(z)$ are measurements of the
total density of the Universe at that redshift; subtracting off the
matter component reveals the dark energy.  This figure shows that
although the fractional importance of dark energy is dropping at
$z>1$, the available precision on $H(z)$ is still sufficient to
make 5\% measurements of the dark energy density.  Our goal is then
to see whether this density is different from today.  If one compares
each point to a known density today and infers the uncertainty on the
power-law exponent of the evolution (parameterized by the familiar
$w$ choice), then one finds a broad maximum in the performance from
$z=0.7$ to $z=2.5$.  The longer redshift baseline at $z>1$ compensates
for the slowly decreasing precision on the dark energy density.
This calculation is just illustrative; it assumes perfect knowledge 
of the matter density and dark energy density at $z=0$ (but these are
likely measurable to levels to make their uncertainties subdominant in the 
comparison).  It also neglects all of the angular diameter distance
information and doesn't combine more than one redshift bin.

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.6\hsize]{DE_logz_4pi.pdf}
\caption{
A simple exploration of the impact of measurements of $H(z)$ from the BAO.
In each redshift bin \textcolor{red}{(represented by the horizontal bars) from Figure \ref{fig:baoforecast}},
we 
map the measurement of $H(z)$ to a measurement
of the dark energy density, having subtracted off the matter density, which
is assumed to be known from the combination of CMB and lower redshift data.
The black points show the fractional uncertainty on the dark energy density, 
which is better than 5\% even at $z\sim 2$.  
\textcolor{red}{The blue arrow shows the fractional uncertainty on that density that would
result from a hypothetical 1\% measurement of $H_0$.}
If we then combine this 
measurement with a known density of dark energy at low redshift, then 
we can infer a power-law evolution, parameterized by the familiar $w$
parameter.  These uncertainties are the red points.  Note that the increasing
lever arm toward higher redshift tends to cancel the decreasing performance,
yielding a broad optimum in the $0.7<z<2.5$ range.
From \cite{Weinberg2012}; see paper for more explanation.
}
\label{fig:density}
\end{center}
\end{figure}

Next, regarding accuracy, the robustness of the BAO method comes primarily from the simplicity
of the early Universe and the relativistic speed of the early sound
waves.  Our theories of the recombination era are exquisitely tested
with the anisotropy measurements of CMB experiments, most notably
the WMAP and Planck satellites and arcminute-scale ground-based
experiments.  From these measurements, we now know the acoustic
scale to better than 1\%, and many possible alterations to the
theory have been sharply ruled out.  Deviations from the adiabatic
cold dark matter theory are now highly constrained!

The relativistic speed of sound before recombination causes the
acoustic scale to be 150 Mpc, which is much larger than the scale
of non-linear gravitational collapse even today.  Large N-body
simulations have found that the shift of the acoustic scale due to
large-scale gravitational flows is only 0.3\% at $z=0$ \cite{seo07,seo10a}; these results
are also found with perturbation theories \cite{padmanabhan09}.  The processes of galaxy
formation occur on scales well below the acoustic scale, but the
small different weighting of the overdense and underdense regions do cause
an additional shift, of order 0.5\% in more extreme cases 
\cite{padmanabhan09,mehta11}.
These shifts are expected to be well predicted by simulations and 
mock catalogs, with uncertainties in the corrections below the statistical
limit of about 0.1\%.

The method of density-field reconstruction aims to sharpen the
acoustic peak by undoing the low-order non-linearity of the large-scale
density field \cite{eisenstein07b,padmanabhan09a}.  Tests in simulations show that reconstruction has
the additional benefit of removing these systematic shifts, providing
another way to suppress systematic uncertainties.

The BAO method is also highly robust as an experimental method.
The core measurements are that of angles and wavelengths, which are
routinely done in astronomy to much higher precision than is required.
The key requirement is the angular homogeneity of the survey, and there
are numerous ways to calibrate this with minimal loss of BAO signal
\cite{vogeley96,tegmark98b,ho08,ross11,ross12}.
Note, however, that the requirements for calibration of the redshift
distortion or Alcock-Paczynski measurements are substantially more 
stringent.

One recently discovered astrophysical effect that can affect the 
acoustic scale peculiarly is the early relative velocity between
baryons and dark matter \cite{tseliakhovich10}.  This velocity modulates
the formation of the smallest early protogalaxies, which may in turn
affect the properties of their larger descendents (although many
practitioners expect the effect at late times to be undetectably
small).  This modulation is due to the same pressure forces that
create the BAO, and the impact could shift the measured acoustic
scale.  However, \cite{Yoo11} shows that this effect also creates
a distinctive three-point clustering signal in the BAO survey data
that allow one to measure the amount of the shift and avoid the systematic
uncertainties.

In summary, we expect that the distance scale information available
in the clustering of matter at $z<3$ can be extracted with sub-dominant
systematic uncertainties even for full-sky ``cosmic-variance-limited''
surveys.  An aggregate precision of 0.1\% allows a remarkable view
of the cosmic distance scale at intermediate redshifts.


\subsection{New projects}

The BOSS project is measuring the BAO at $z<0.7$, with a first look 
at $z>2$.  However, this leaves substantial swaths of cosmic volume
to be surveyed in the coming decade.  New galaxy surveys will focus
on $0.7<z<3$, while a much denser grid of quasars will substantially
improve the constraints from the Lyman-$\alpha$ forest.

The primary DOE-led project is DESI.  This proposes to use a new 
wide-field spectrograph on the Mayall 4-m telescope
at Kitt Peak to perform a massive new survey of 35 million
galaxies over 18,000 square degrees.  This will include a dense
sample of emission-line galaxies at $0.7<z<2$ as well as a sharply
improved sampling of the Lyman-$\alpha$ forest beyond $z=2$.

eBOSS within SDSS-IV will build on the successful SDSS-III BOSS
program.  It will use the existing instrument with new targeting
to provide a first percent-level look at BAO in the $0.7<z<2$ redshift
range, using a sample of quasars and galaxies.  It will also improve the sampling
of the Lyman-$\alpha$ forest.  eBOSS will continue to build the
momentum of BOSS toward DESI science, providing an improved data
set for continued development of the large-scale structure methodology
as well as key training sets for DESI target selection.

The HETDEX project is building a large set of integral-field
spectrographs for the Hobby-Eberly Telescope.  With these, they
will conduct a blank-field search for bright Lyman-$\alpha$ emitting
galaxies at $1.9<z<3.8$.  The initial survey will start in 2014 and
cover 400 square degrees, with a sampling density that is somewhat
sparse but still useful for BAO.  We know of no planned extension of
this technique to $10^4$ square degrees. 
We believe that the Lyman-$\alpha$ forest method is the more efficient
route to the cosmic variance limit at these redshifts.

The PFS project is building a wide-field fiber spectrograph for
Subaru.  It will survey about 1500 square degrees to fainter levels
than DESI.  While this is enough to measure a BAO signal, the major
goals of PFS are in galaxy evolution and smaller-scale clustering.
DESI's much wider survey area is better optimized for BAO and
large-scale clustering.

Euclid is a European Space Agency mission to study dark energy.  It
will perform slitless spectroscopy in the near-infrared over 15,000
square degrees, reaching to $z=2$ but with most signal in the
$0.7<z<1.5$ range.  \textcolor{red}{The satellite is scheduled to launch in 2020 with
a six-year mission lifetime.}

WFIRST is a NASA project.  It will provide a wide-field slitless 
spectroscopy capability.  Likely it will aim for denser samples than 
Euclid over somewhat smaller areas.  WFIRST is currently planning
to launch in the early- to mid-2020's.

The major imaging surveys such as DES and LSST will measure BAO
using photometric redshifts.  However, the lack of redshift precision
causes the BAO signal to be blurred out, particularly along the
line-of-sight.  This is a substantial loss to the constraints on
$D_A$ and a total loss of $H(z)$.  The Spanish PAU and JPAS projects
aim to remedy this by using medium-band filters to obtain higher
photometric redshift precision, at a substantial loss in imaging
depth.

An intriguing recent idea is to study the BAO at radio wavelengths
using the 21 cm line.  In most cases, one does not resolve individual
galaxies but instead measures the 3-dimensional large-scale structure
at $>10'$ scale with large interferometric arrays.  This is known
as intensity mapping.  The CHIME project aims to build a 100 meter
square filled interferometer using a cylindrical telescope array
\cite{peterson06} and conduct a lengthy survey at $0.8<z<2.5$.  If
the foregrounds can be adequately controlled, CHIME would be a
powerful demonstrator of the 21 cm method and would yield excellent
cosmological information.  Other projects include the FFT-based
Omniscope \cite{tegmark10} and the Baryon Acoustic Oscillation
Broadband and Broad-beam (BAOBAB) interferometer array \cite{pober1210.2413}.
The challenge in intensity mapping is the removal of other radio emission,
which is about $10^4$ brighter, but expected to be spectrally smooth.

Moving beyond intensity mapping, the SKA could enable an HI-redshift
survey of a billion galaxies, reaching the sample variance limit
over half the sky out to $z=3$ \cite{abdalla05}, which would be a
good approximation to the ultimate BAO experiment.


\subsection{Beyond BAO}

The acoustic peak may be the headliner from these cosmological surveys,
but it is far from the only source of information from redshift
surveys designed to study cosmology and dark energy. 

Modifications of gravity can be tested by the growth of structure as
revealed by the correlation of peculiar velocities and densities
known as redshift-space distortions  \cite{2013arXiv1309.5385H}.  The total mass of neutrinos can
be measured by the tilt of the clustering power spectrum \cite{2013arXiv1309.5383A}.  
Non-Gaussianity from inflation can be measured either in two-point
clustering from the largest scales or in higher-point clustering \cite{2013arXiv1309.5381A}.
And in the largest samples, we may improve upon the CMB measurements
of the matter density and spectral tilt \cite{2013arXiv1309.5381A}.

These items are discussed in contributed white papers.  Here, we discuss an
opportunity that is specific to the cosmic distance scale, namely the 
Alcock-Paczynski (A-P) effect \cite{alcock79}.  This is simply the idea that an intrinsically
spherical object or pattern in the Hubble flow will appear ellipsoidal 
if one assumes the wrong cosmology.  From this, one measures the
product of the angular diameter distance and the Hubble parameter.
As with the BAO, this is of particular interest at higher redshift, where
the $H(z)$ information more directly reveals the density of dark energy.
The BAO method itself is an example of the A-P effect, but with an object
of known size.  

The A-P effect requires the object to be in the Hubble flow, so we
generally focus on large-scale clustering, whether in the two-point
function or non-Gaussian effects such as the shapes of voids
\cite{ryden95,ballinger96,matsubara96,popowski98,hui99,mcdonald99,matsubara01,lavaux10,sutter12}.
In principle, the A-P effect can be measured more precisely than
simply the ellipticity of the BAO ring.  This is because the A-P
effect is a modulation of the broad-band clustering, rather than
the weak oscillation of the BAO, and because the A-P effect can be
measured to higher wavenumbers, where there are lots of modes.

The challenge of the A-P effect is that it is partially degenerate
with the apparent ellipticity of clustering caused by redshift-space
distortions.  The two effects do have distinct dependences on
wavenumber, so with a sufficient range of scales one can separate
them \cite{matsubara04}.  However, the redshift-space distortions are of order unity
ellipticity, whereas the precision of the BAO ellipticity is expected
to be below 1\% and perhaps reach 0.3\%.  Hence, for the broadband
A-P effect to improve on the BAO information, we need to remove the 
redshift-distortion effects to better than one part in 100.  This is
a significant modeling challenge.  There are also observational 
systematics, as the surveys must calibrate the amplitude of their 
radial and transverse clustering measurements.

Nevertheless, the data sets to be taken for BAO measurements will
permit the A-P analyses to proceed.  Furthermore, the modeling
required is the same as for the redshift-distortion measurement of
growth of structure.  If the modeling can succeed, then the A-P
effect can notably improve the $H(z)$ measurements at $z>1$.



