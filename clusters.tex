\subsection{Clusters}

Complementing other cosmological tests based on the mass
function and clustering of galaxy clusters \textcolor{red}{(e.g. 
\cite{Allen1103.4829,Weinberg1201.2434})}, nature offers two
independent ways of using clusters to measure cosmic distances.  The
first uses measurements of the X-ray emitting gas mass fraction in the
largest clusters, which is an approximately standard quantity,
independent of mass and redshift. The second uses combined mm and
X-ray measurements of cluster pressure profiles.

\subsubsection{Distance measurements from the cluster 
X-ray gas mass fraction}


\paragraph{Overview and current status}

The largest clusters of galaxies provide approximately fair
samples of the matter content of the Universe. This enables X-ray
measurements of their baryonic mass fraction (the baryonic mass is
dominated by the X-ray emitting gas) to provide robust, and
essentially model-independent, constraints on the mean matter density
of the Universe \cite{White93}.  Additionally, measurements of the
apparent evolution of the cluster X-ray gas mass fraction, hereafter
$f_{\rm gas}$, can be used to probe the acceleration of the Universe
(\cite{Allen0405340,Allen0706.0033}).  This latter constraint
originates from the distance dependence of the $f_{\rm gas}$
measurements, which derive from the observed X-ray gas temperature and
density profiles, on the assumed distances to the clusters, $f_{\rm
gas} \propto d^{1.5}$.

\fgas{} measurements can be determined from X-ray observations under
the assumptions of spherical symmetry and hydrostatic equilibrium. To
ensure that these assumptions are as accurate as possible, it is
essential to limit the analysis to the most dynamically relaxed
clusters.  A further restriction to the hottest, most X-ray luminous
systems simplifies the cosmological analysis and minimizes the required
exposure times. The state of the art for this work \cite{Mantz13}
employs Chandra measurements for a sample of 40 of the hottest
($kT_{2500}> 5$\,keV), most dynamically relaxed clusters known. The
selection on dynamical state is applied automatically, based on X-ray
images.  The \fgas{} measurements are made within a spherical shell
spanning the radial range 0.8--1.2\,$r_{2500}$, for a given reference
cosmology (angular range 0.8--1.2\,$\theta^{\rm ref}_{2500}$).

Combining the two aspects discussed above (fair sample and distance
dependence) the cosmological model fitted to the $f_{\rm gas}(z)$ data
typically has the form

\begin{equation}
  f^{\rm ref}_{\rm gas}(z;\theta^{\rm ref}_{2500}) = 
K\, A\, \Upsilon_{2500}\,\left(\frac{\Omega_{\rm b}}{\Omega_{\rm m}}\right)
\left(\frac{d^{\rm ref}_{\rm A}}{d_{\rm A}}\right)^{3/2}\,.
\label{eq:fgas3}
\end{equation}

Here $K$ encompasses the main systematic uncertainties,
associated with instrumental calibration and the accuracy of the
hydrostatic mass measurements. Fortunately, $K$ can be constrained
robustly through combination with independent weak lensing mass
measurements \cite{Applegate13b}. The angular correction factor, $A$,
accounts for the fact that $\theta^{\rm ref}_{2500}$ for the reference
cosmology and $\theta^{\rm }_{2500}$ for a given trial cosmology are
not identical. $\Upsilon_{2500}$ is the gas depletion parameter, the
average ratio of the cluster gas mass fraction to the cosmic mean
baryon fraction at the measurement radii, as predicted by
hydrodynamical simulations
(e.g. \cite{Planelles1209.5058,Battaglia1209.4082}).  Priors on the
Hubble parameter, $h$, and the mean baryon density $\Omega_bh^2$ are
also required to constrain cosmology from \fgas{} data alone.

Fig.~\ref{fig:simres} shows the cosmological constraints from current
\fgas{} data (red curves; \cite{Mantz13}) for non-flat \LCDM{};
flat, constant-$w$; and evolving $w$ models. In all cases the
results are marginalized over conservative systematic uncertainties.
The \fgas{} data provide comparable constraints on dark energy
to current SNIa measurements \cite{2012ApJ...746...85S}, and an
impressively tight constraint on $\Omegam$ independent of the
cosmological model assumed \cite{Mantz13}.


\begin{figure}[t] \centering
  \begin{minipage}[c]{0.33\textwidth}
    \includegraphics[width=\textwidth]{lcdm__om_ol__projections.pdf}
  \end{minipage}%
  \begin{minipage}[c]{0.33\textwidth}
    \includegraphics[width=\textwidth]{w__om_w__projections.pdf}
  \end{minipage}%
  \begin{minipage}[c]{0.33\textwidth}
    \includegraphics[width=\textwidth]{wa__w_wa__projections.pdf}
  \end{minipage}%
  \caption{Joint 68.3\% and 95.4\% confidence constraints from the
\fgas{} method for non-flat \LCDM{} (left), flat $w$CDM (center) and
flat evolving-$w$ (right) models. The red contours show the
constraints from current \fgas{} data (\cite{Mantz13}; also
employing standard priors on $\Omegab h^2$ and $h$). The blue contours
show the improved constraints expected with the addition of an extra
10\,Ms of Chandra observing time, together with modest improvements in
external priors \cite{2013arXiv1307.8152A}.}
\label{fig:simres}
\end{figure}


\paragraph{Prospects for improvement}

New optical, X-ray and mm-wave surveys over the next
decade will find in excess of 100,000 clusters, including thousands of
hot, massive systems out to high redshifts. The hottest, most X-ray
luminous and most dynamically relaxed of these will be the targets for
further, deeper observations designed to enable the \fgas{} (and XSZ;
see below) experiments.

The blue curves in Fig.~\ref{fig:simres} show the expected
improvements in the cosmological constraints from the \fgas{} method
over the next decade, using current techniques and assuming that a
further 10\,Ms of Chandra observing time ($\sim 5$\% of the available
total) will be invested in this work. (These predictions also assume
modest improvements in the external priors on $K$, $\Upsilon_{2500}$,
$\Omega_bh^2$ and $h$; see \cite{2013arXiv1307.8152A} for details.) Observations
of this type are also likely to be interesting for a broad range of
other astrophysical and cosmological studies.

The availability of a new X-ray observatory with comparable spatial
resolution to Chandra and a collecting area $\sim 30$ times larger
would \textcolor{red}{likely enable a reduction in the area of the
confidence contours shown} in Fig.~\ref{fig:simres}(c) by a further
factor of $\sim 7$ \textcolor{red}{(for details see
\cite{2013arXiv1307.8152A})}. Possibilities include the SMART-X
mission (http://hea-www.cfa.harvard.edu/SMARTX/) and Athena+
\cite{Nandra1306.2307}.\footnote{\textcolor{red}{The Athena+ science
theme “The hot and energetic Universe” was recently selected for the
second Large-class mission in ESA’s Cosmic Vision science program.}}


\paragraph{Key challenges}

The key challenges to realizing the prospects described above will be
securing the required observing time on flagship X-ray observatories,
and delivering the continued, expected improvements in $K$ and
$\Upsilon_{2500}$ from weak lensing studies and hydrodynamical
simulations, respectively \cite{2013arXiv1307.8152A}.


\subsubsection{Distance measurements from SZ and X-ray pressure profiles}


\paragraph{Overview and current status}

Cosmic microwave background (CMB) photons passing through a galaxy
cluster have a non-negligible chance to inverse Compton scatter off
the hot, X-ray emitting gas.  This scattering boosts the photon energy
and gives rise to a small but significant frequency-dependent shift in
the CMB spectrum observed through the cluster known as the thermal
Sunyaev-Zel'dovich (hereafter SZ) effect \cite{Sunyaev72}.

It has been noted \cite{WhiteSilk78} that X-ray and SZ measurements can be
combined to determine distances to galaxy clusters.  The spectral
shift to the CMB due to the SZ effect can be written in terms of the
Compton $y$-parameter, which is a measure of the integrated electron
pressure along the line of sight ($y \propto \int n_{\rm e}\, T\,
dl$). This shift, $y_{\rm SZ}$, is
independent of the cosmology assumed.  A second, independent
measure of the $y$-parameter can also be obtained from X-ray data
where, for a given reference cosmology, the X-ray measurement, $y_{\rm
X}^{\rm ref}$, depends on the square root of the angular diameter
distance to the cluster. Combining the results, we obtain

\begin{equation}
{y_{\rm X}^{\rm ref}} = {y_{\rm SZ}^{\rm }}\, k(z)\left( \frac{d_{\rm A}^{\rm ref}}{d_{\rm A}^{\rm }}\right)^{1/2}\,.
\label{eq:ycompton}
\end{equation}

Due to the modest distance dependence of this method, and the
limitations of the available data, this test (sometimes referred to as
the XSZ or SZX test) has to date only been used to constrain $h$, with
all other cosmological parameters fixed.  Assuming spatial flatness
and fixing $\Omega_{\rm m} = 0.3$, \cite{Bonamente0512349} applied
this method to 38 X-ray luminous clusters, finding $h =
0.77^{+0.11}_{-0.09}$.

\paragraph{Prospects for improvement}

Over the next decade, utilizing X-ray measurements for $\sim 500$
clusters from the eROSITA satellite (measurements of a type that will
be gathered by default by that satellite for its central science
goals), in combination with follow-up SZ measurements from e.g.
CARMA\footnote{http://www.mmarray.org/} or
CCAT\footnote{http://www.ccatobservatory.org/}, the XSZ technique can
be expected to provide cosmological constraints similar to those shown
in Fig.~\ref{fig:xszsim}. With this expanded data set, and assuming
plausible levels of uncertainty in the X-ray and SZ instrument
calibration, interesting constraints on $h$ should be achievable for
non-flat \LCDM{} and $w$CDM models, solving simultaneously for
$\Omegam$ and $\Omegal$ and/or $w$ \cite{2013arXiv1307.8152A}.


\begin{figure}[t] \centering
  \begin{minipage}[c]{0.49\textwidth}
    \includegraphics[width=0.9\textwidth]{xsz_projection__om_h.pdf}
  \end{minipage}%
  \caption{Joint 68.3\% and 95.4\% confidence constraints on $\Omegam$
and $h$ for a flat \LCDM{} model using simulated XSZ data for 500
clusters, as described in the text.
}
\label{fig:xszsim}
\end{figure}

\paragraph{Key challenge}

The key challenge to realizing the prospects for the XSZ experiment
described above will likely lie in providing precise, robust absolute
calibrations for the X-ray and SZ $y$-parameter measurements
\cite{2013arXiv1307.8152A}.

