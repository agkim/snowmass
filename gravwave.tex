\subsection{Gravitational Wave Sirens}

\textcolor{red}{We are on the verge of a new era of gravitational wave (GW) astronomy.  It is
widely expected that the coming few years will witness the first direct
detection of GWs. A network of ground-based observatories, composed of the LIGO
and Virgo detectors, are currently being upgraded to ``advanced''
sensitivity. Once operational, these detectors are expected to observe a
significant stellar mass compact binary merger rate (perhaps dozens per year;
e.g., \cite{2010CQGra..27q3001A,2013ApJ...779...72D}).  All being well, the second half of the
current decade should see routine ground-based GW observations of binary
coalescences.  The launch of a space based GW antenna would extend
the GW window to low frequencies and probe processes involving supermassive ($M
\gtrsim 10^5\,M_\odot$) black holes.  GWs were recently selected as part of the ESA
cosmic vision science program, with a nominal launch date of a mission such as
eLISA in 2034.
}

Standard sirens are gravitational wave sources for which the absolute
luminosity distance and redshift can be determined, and are thus the GW analog
to standard
candles~\cite{1986Natur.323..310S,2002luml.conf..207S,2005ApJ...629...15H,2006PhRvD..74f3006D,2009PhRvD..80j4009C,2010PhRvD..81l4046H,2010ApJ...725..496N,2011ApJ...739...99N,tempref}.
Standard sirens are of particular interest because they take advantage of the
simplicity of black holes (which are understood from a basic physics
perspective, and are {\em fully}\/ described by mass, spin, and charge) to
provide an absolute distance, allowing measurements to cosmological scales
without the use of distance-ladders or phenomenological scaling relations.

As first pointed out by~\cite{1986Natur.323..310S,2002luml.conf..207S}, by measuring
the gravitational waveform during the inspiral and merger of a binary
it is possible to make a {\em direct and absolute}\/ measurement of
the luminosity distance to a source. 
This is because the physics underlying the inspiral of a binary due to
GW emission is well described and understood in
general relativity.
These sources thus offer an entirely
independent and complementary way to measure the evolution history
of our Universe. Standard sirens are physical, not astrophysical,
measures of distance.

Multi-messenger astronomy is crucial to unlock the power of these
binary sources. An important limitation of GW binaries
is that they do not provide the redshift to the source; the
redshift is degenerate with the binaries' intrinsic parameters.
However, if an electromagnetic counterpart to a binary
inspiral event is identified, it is then possible to measure the
redshift independently.\footnote{As
emphasized by \cite{1986Natur.323..310S,2012PhRvD..86d3011D}, it is also possible
to do this in a statistical fashion, rather than identifying
redshifts of individual sources.}
In addition, by determining the exact location of the
source on the sky, the fit to distance is significantly improved.  In
short, gravitational waves provide absolute distance while
electromagnetic measurements provide redshift, and the combination makes it possible to
put a very accurate point on the distance--redshift curve.  A binary
inspiral source coupled with an electromagnetic counterpart would
therefore constitute an exceedingly good cosmological standard
siren.

There are three standard siren cases of particular interest: 1. stellar mass
compact binaries detected by ground-based GW observatories, 2. supermassive
binary black holes detected by space-based GW observatories, and 3. stellar mass
compact binaries detected by space-based GW observatories. We discuss them
briefly below:

{\bf 1.}
The advanced LIGO/Virgo network is expected to begin operating within the next
few years. One of the most promising sources for
these ground-based observatories is the inspiral and merger of
binaries consisting of neutron stars and/or black holes. These sources will be
detected to $\sim350\,\mbox{Mpc}$, and the event rates are thought to be in the
range of $0.1$--$1000/\mbox{year}$ for both double neutron star and neutron
star-black hole systems~\cite{2010ApJ...715L.138B,
  2010ApJ...708..117B,Abadieetal:2010,2012ApJ...749...91F,2012ApJ...757...91B,
  2012ApJ...759...52D,aasi:2013}. There has been much activity recently exploring the
possibility of electromagnetic counterparts to these events. For example, it is
conceivable that the radioactive powered ejecta of r-process elements will
produce an isotropic optical/infrared counterpart to the merger (a
``macronovae'' or ``kilonovae'').

A particularly exciting multi-messenger source for ground-based GW observatories is the
inspiral and merger of a stellar-mass compact binary associated with a
short/hard gamma-ray burst (GRB). We know these GRBs exist and that
they occur in the local Universe. 
In addition, there is growing evidence that they
are associated with binary inspiral (i.e., double neutron star or
neutron star--black hole systems), and if this is the case, then we know that they would
be detectable by a network of ground-based GW detectors with advanced
LIGO sensitivity \cite{2013arXiv1307.6586L}.

\begin{figure}
\centering 
\includegraphics[width=0.98\columnwidth]{./fig2H0new-eps-converted-to.pdf}
\caption{$H_0$ measurement uncertainty as a function of the number of multi-messenger
  (GW+EM) double neutron star merger events observed by an advanced LIGO-Virgo
  network. The dashed line shows Gaussian convergence.}
\label{fig:H0}
\end{figure} 

If these sources are observed in both the GW and the EM spectra, we can ask how
well they can be used as standard candles~\cite{2006PhRvD..74f3006D,2010ApJ...725..496N,tempref}.
Fig.~\ref{fig:H0}, taken from~\cite{tempref}, shows that
20 events would provide a 3\% measurement of $H_0$. This improves to 1\% if the 
sources are beamed, as would be expected for GRBs.
Although, as was shown in~\cite{2010ApJ...725..496N},
the distance-inclination degeneracy compromises the utility of individual
sources, the posterior probability distribution function for an ensemble of
sources sharply mitigates this. This arises precisely because of the strong non-Gaussianity in the
distance uncertainties due to the degeneracy.
This measurement of $H_0$ would
provide one of the best, and of greater import, one of the cleanest and most
direct estimates of the age of the
Universe.

{\bf 2.}
Space-based gravitational wave observatories
are sensitive to supermassive binary black hole coalescences out to
very high redshift ($z\sim10$). If we are able to independently
determine redshift, we could use these systems as standard sirens to
provide a powerful complementary cosmological
probe~\cite{2005ApJ...629...15H}. Weak lensing compromises the utility of
individual sources at high redshift, but for sufficient statistics the lensing
effects average out, and precision cosmology is possible.

{\bf 3.} Extrapolating to the distant
future, a space-based decihertz gravitational-wave mission, such as
the Big Bang Observer, would constitute a truly revolutionary
cosmology mission, measuring the equation-of-state of the dark energy
and the growth of structure to a precision comparable to that of all other
proposed dark energy/cosmology missions {\em
  combined}~\cite{2009PhRvD..80j4009C}.
