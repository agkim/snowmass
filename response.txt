Response to Reviewers

------RESPONSE--------

Please note that non-trivial changes are highlighted in red in the new
submission.

----------------------

Reviewers' comments:

Reviewer #1: Report on 'Distances', a report from the Snomass 2013 meeting,
corresponding author Alex Kim.

This is a somewhat odd thing to review -- it is essentially meant to
be a record of the discussion at a particular conference rather than
current research, and so it isn't clear what changes are fair game 
for me to recommend.  It is, however, clear that this provides a useful
snapshot of current thinking in the field, and hence should be published.

I would like to see the revised version of the paper, but really only
to understand the first question below.

Overall:
1) My first comment/question -- which may really be for the editor --
is how much context from other Snomass reports can be assumed?  In
particular, one thing that stood out from this report is that there is
not much effort to justify -why- one would want to measure distances over
particular redshift ranges.  For example, there is discussion about
extending the BAO and SN measurements to higher redshift, but there
isn't a large amount of context for why that is interesting.  Perhaps
that is adequately covered by other Snomass reports -- although even
if it is, it might be useful to add some pointers to those discussions
in the introduction.

------RESPONSE--------

We have added the following paragraph justifying the redshift range.

The cosmic expansion history is a fundamental element of the physics
of our Universe. Ideally we would map it accurately at all
redshifts. Within a cosmological model such as cold dark matter plus
dynamical dark energy, the precision on the dark energy equation of
state $w(z)=w_0+w_a z/(1+z)$ starts to plateau for measurements beyond
$z\approx1.5-2$. However, even within a cosmological constant model
the dark energy contributes nearly 10\% of the cosmic energy density
at $z=2$ and alters the deceleration parameter by 25\%. Surprises
could certainly await as we probe to these redshifts and beyond. Thus
next generation experiments aim to map cosmic distances to
$z\approx2$, as outlined in the Rocky III report, while keeping in
mind potential techniques to improve our understanding further.

----------------------

2) A failing of the discussion -- which seems common in this type of
report -- is that well established methods are subject to a large amount
of analysis and criticism, while 'new' methods are presented in a very
optimistic light.  I would find it quite helpful to have a bit more
information about the main difficulties that each probe in section 4
will have to overcome rather than just projections of how much
better they expect to do in the future.  Right now there's essentially
one attempt to inject a little caution (in the introduction), and then
a river of pure optimism.  

 For example, nowhere in 4.3 is it explicitly pointed out that
gravitational waves have not yet been directly detected at all, and
so the source rates are incredibly uncertain.  Plus, people think
they have modelled these things well, but without any real data,
there will probably be a few surprises.  Yes, both should be obvious
to the reader, but still...  And evolution is a major concern for the
AGN radius-luminosity relation and for cluster measurements.

Pointing out such limits doesn't have to be overly negative -- in
fact, studiously ignoring any potential issues only makes the reader
less likely to believe any of the claims.

------RESPONSE--------

For each of the "new" methods we included or have added text on key
challenges that need to be overcome in order for the method to be
successful.

For clusters this can be found at the end of 5.1.1 and 5.1.2.

For strong lensing a new paragraph is added to the end of 5.2.

For gravitation waves, a new first paragraph in 5.3 reviews the planned
progression of gravitational wave experiments and their source
detections.

For reverberation mapping, the challenges that need to be addressed
are given in the second paragraph of 5.4.2.

----------------------


3) The section on complementarity between BAO and SNe Ia at the
 end of the BAO section doesn't flow very well, since SN Ia haven't
 really been discussed.  I would recommend moving discussion of
 complementary to a new section inserted after the SN discussion.

------RESPONSE--------

As suggested, the discussion is moved to a new section after the SN discussion.

----------------------


4) This could have used some editing.  There are a lot of sentence
fragments and unfinished statements -- enough that this reader, at
least, found them quite distracting, particularly in the
introduction.  That style of writing can work well in fiction, but
here the effect is to make the paper read more like a transcription
of notes than proceedings.  I will try to list some of the problems below,
but I have no doubt that there are many remaining. 

------RESPONSE--------

The introduction has been edited resulting in what we think is a
smoother read.  Incomplete sentences, fragments, and many commas have
been removed.

----------------------

Major:
Figure 1 is just about unreadable (and the caption needs serious help).
If the authors really want to show this much detail, they probably
need to split the left and right into individual full-width figures
while decreasing the relative symbol sizes.


------RESPONSE--------

We have increased the figure sizes in latex.  We believe that there is
benefit in having the figures next to each other, to emphasize and
give the reader a single-glance view of the two distances that SNe and
BAO will measure.  We will work to ensure readability in the proof
stage.

We have trimmed the caption, moving the details into text within the
article.

----------------------

 There is a lot of discussion about 'absolute' luminosities in the
supernova section.  As the authors are fully aware, this is not
a requirement of SN cosmology, so these should be carefully reworded
to avoid 'absolute luminosities', 'absolute magnitudes', etc.
Perhaps it is worth spending a sentence explaining that 'absolute'
luminosities wash out in the cosmography.


------RESPONSE--------

We add the following sentence in the second paragraph of Section 3.2 Context:

Relative measurements of luminosity distance as a function of
redshift, which do not require knowledge of intrinsic supernova
luminosities, constrain the energy densities and equation of state of
dark energy; absolute measurements of luminosity, which do require
knowledge of intrinsic luminosities, constrain the Hubble parameter
$H_0$.

Note that the article also discusses measurement of the Hubble
parameter with supernovae.

-----

page 17, p 33: not a paper comment, but... 10,000 real-time spectroscopically 
    followed SNe.  Good luck with that!

    More seriously, there is a mismatch between the statement that 
    R~100-1000 is needed for LSST sources and the R=75 provided by the 
    WFIRST reference mission.  This needs to be explained.

------RESPONSE--------

In the LSST section, we replace R~100 - 10000 with $R>~100$.

In the WFIRST section, we add the following text:

The resolution of the IFU is designed to be R = 75, a solution that
balances the degradation of measured supernova spectral features with
a reduction of non-negligible detector noise.

-----


Minor:

page 2, line 50: I question whether the DESI distance measurements
are fully independent.  Are the authors really stating that the 
off-diagonal covariance terms will be identically zero?  I would
remove 'independent' here.

------RESPONSE--------

The measurements are statistically independent.  As discussed in the BAO
text, the systematic errors are expected to be sub-dominant to the
statistical errors, even in the limit that the entire sky is observed.
We are therefore comfortable with the statement that the DESI distance
measurements are independent.

-----

page 3, line 51: While the detailed discussion does cover projects
 like HETDEX, and previous ones like wiggle-z, somehow the executive
 summary here reads as a rather biased take reflecting the projects
 that the people who were actually at Snomass are involved in.  I
 recommend mentioning HETDEX and PFS here.

 On a related note, is there some citation to back up the claim that
 the Lya forest is a lot more efficient than Lya emission galaxies?
 That's a strong enough statement it would be good if it were backed
 up a bit somewhere.

------RESPONSE--------

The first item is a good point, and we've decided to take the opposite
approach to that proposed by the referee.  Instead of mentioning
surveys by name, we simply state the following: "Surveys in the coming
decade will extend our view at $z>0.7$ by over an order of magnitude
in cosmic volume."

As for the second point, there are many ways to support the statement
that lya forest is more efficient that lya emission galaxies.  For
one, the BOSS BAO measurement has 1% precision in the CMASS sample and
2% precision in the lya forest sample.  Both samples are tied to the
same exposure sequence, but we use almost a full order of magnitude
larger sample of galaxies than lya quasars (1.3M vs 175,000).  The lya
emitting galaxies will be much less efficient than the CMASS galaxies
because they are typically a 10X fainter, so they will be less
efficient than lya forest as well.  Factoring in selection efficiency
will probably make the case even stronger for lya quasars.  For
simplicity, we address this comment with a reference to the paper that
makes first projections for lya forest constraints, with some
comparison to galaxies.  In that paper, we find comparable constraints
from 1000 galaxies per sq degree as from 40 lya forest quasars per sq
degree.  The reference to McDonald and Eisenstein has been added.

-----

page 6, line 55: 'misweighting' is an odd choice. Rather than applying
  some value judgement, just say 'different weighting' or something.

  Figure 2: on the y axis, Fractional Uncertainty in what?
    I don't want to have to look at the caption to know.  Actually,
    the caption doesn't say either.
  Please note in the caption that the horizontal bars are bin sizes,
    not uncertainties.

------RESPONSE--------

'misweighting' changed to 'different weighting'

The fractional errors on are DA(z) and H(z), with different symbols for each.
The symbols are denoted on the bottom right hand side of the figure.
We have updated the caption to state this and clarified that the horizontal
bars are bin sizes and not uncertainties.

-----

page 7, figure 3:
  Please note in the caption that the horizontal bars are bin sizes,
    not uncertainties.
  What is the 1% H_0 value on the left?  Please explain in the caption,
    or remove it.

------RESPONSE--------

Bin sizes noted in caption.  We also added the following statement to the 
caption to clarify the confusion WRT 1% H0:
"The blue arrow shows the fractional error on that density that would
result from a hypothetical 1\% measurement of $H_0$."

-----

page 8, line 49: please provide a timeline for Euclid, and italicize
   if appropriate.  Also italicize WFIRST if appropriate for this journal.

------RESPONSE--------

We have added the following to the discussion of Euclid:
"The satellite is scheduled launch in 2020 with
six-year mission lifetime."

As for formatting, it is not clear what the journal requirements are, the
author guidelines simple state:  "There are no strict formatting requirements..."

-----

page 10: Unfortunately, there isn't really anything unprecendented
  about the redshift depth of either LSST or WFIRST.  Now, the number
  of z ~ 1.7 SNe Ia WFIRST hopes to find is orders of magnitude better
  than PANS or CANDELS, it's not like there is much discussion of
  finding, say, z = 3 SNe Ia.  It's really the numbers in this 
  slightly explored redshift range that are unprecendented here.

------RESPONSE--------

The flux calibration goals of LSST and WFIRST, and the wavelength
coverage of WFIRST from 0.4 to 2 microns, will lower current error
floors.  This feature is as important as getting sheer numbers of
supernovae.

------


page 11: line 51: current low-z surveys should include KAIT at a minimum
               (unless they've stopped; the authors know more about this
		than I do), probably CSP, CfA.

------RESPONSE--------

We were limiting ourselves to searches that are dedicated to
cosmology, in particular those that study supernovae in the Hubble
flow.  This criterion leaves out KAIT, SkyMapper, and Assassin.

-------

page 12: line 28: 'unexpected dust absorption' -> may not be dust, 
             as the example used (the SNFactory feature thing) probably	
	      indicates.  Best to just say 'unexpected nature of the 
	      observed relation between color and luminosity' or something.
	      Maybe slope rather than nature.  

------RESPONSE--------

This has been reworded.

Some of the unexpected color corrections ascribed to dust derived from
broad-band color analysis

-------

        line 33: 'All these reductions...evolution with redshift no longer
           enters as a source of systematic uncertainties.'  Umm.. what?
           There is nothing here to say that they will no longer enter
           as systematics, just that maybe we will be able to put a 
	    more accurate number on any systematics.  The SNFactory
	    spectral feature is a good example -- if we can't measure
	    that at high-z, then we won't be able to eliminate this as
	    a redshift dependent systematic, just try to put a better number
	    on what we might be missing.  That might reduce the systematic
	    slightly, but hardly makes it either go to zero or loose all
	    redshift dependence.

------RESPONSE--------

Parsing the sentence, "for which" refers to "intrinsic parameters",
not "reductions".

-------

page 15, line 33: KAIT should probably be on this list, although I suppose
     one could try to argue cosmological application is not its primary
     purpose.

------RESPONSE--------

The low-redshift supernovae discovered by KAIT and others are relevant
to understanding the standard candle but are not easily associated
with distance since peculiar velocities contribute significantly to
their redshifts.

-------


page 16, line ~ 27: Later in this section there is discussion of
     getting host redshifts, but this isn't really linked to the 'deep LSST'
     sample.  Presumably host-z follow up would start with those fields
     at least.

------RESPONSE--------

The discussion here is generic, so applies to supernovae in both main
and deep drilling fields.

----


     One issue not mentioned -- and which maybe wasn't discussed at
     Snomass -- is that the cosmological analysis will require some work
     to deal with photo-zs at the target level of precision.  That is, even
     if the photo-z and photo-types can be developed to the point where
     their failures are well characterized, there will probably need to
     be some work done in understanding how to fold in things like
     catastrophic photo-z outliers into the analysis, since there is no
     reason to expect them to average out nicely over the sample.
     It's a minor point, but it's important to note that just knowing
     what fraction of your 'SNe Ia' are really, say, SNe Ic is not enough --
     you need to be able to propogate that into your cosmology.

------RESPONSE--------

Text has been added so the text reads:

Spectroscopic confirmation will be required for a subset of
2,000–10,000 LSST SNe. This unbiased sample is necessary to quantify
the systematic bias incurred when spectroscopic information is
unavailable.

----

page 18: The number of WFIRST fields/pointings/area covered is not
   given for the reference WFIRST-2.5 plan.  Please add that
   information.

------RESPONSE--------

The information is now added to the text:

There are three tiers to the survey: a shallow survey over 27.44 deg2
for SNe at z < 0.4, a medium survey over 8.96 deg2 for SNe at z < 0.8,
and a deep survey over 5.04 deg2 for SNe out to z = 1.7.

------

page 22, line 12: An important point is that the LSST transients will
      also extend -much- fainter than PTF transients, not just that
      there will be more of them.

------RESPONSE--------

The numbers refer to discoveries at magnitudes that can be followed up
at up to 10-m telescopes.  The wordking has beend modified.

While current surveys like PTF generate one to two such candidates a
night — and can be followed by 4–10m class telescopes, LSST has the
potential for finding hundreds of candidates to feed telescopes of
these apertures per night.

------

page 24: It is strange that the authors cite unpublished analyses
   but not the published Vikhlinin series of cluster X-ray papers from 2009,
   which seem to be the -published- state of the art.

------RESPONSE--------

Vikhlinin et al. (2009) do not exploit the distance measurement
techniques discussed here (fgas and XSZ), but rather measurements of
the mass function and its evolution (a.k.a. cluster counts).  The
state of the art for cluster counts is represented by Vikhlinin et
al. (2009), Mantz et al. (2010), Rozo et al. (2010) and Benson et
al. (2013). A couple of review articles, discussing cluster count
measurments and their prospects, are now cited in the first sentence
of the opening paragraph.

The Mantz et al. (2014) analysis was submitted to MNRAS and received a
favorable referee report. It will be on the arXiv and in press soon.

----------------------

page 25: Presumably when the authors state that, say, ATHENA+
   would improve the blue contours by a factor of 7 (and in what,
   by the way? area?), they are assuming statistical unceratinties
   only.  If so, this must be qualified as such.

------RESPONSE--------

Yes, the area of the contours would shrink by this factor. The
predictions include systematic as well as statistical uncertainties,
as described in the cited article (Allen et al. 2013) from which these
extracts are drawn. This is now clarified in the text.

---------------------

   I'm not sure if it's worth pointing out that ATHENA+ was actually
   selected by ESA recently.  That wasn't the case at the time of
   Snomass, however.

------RESPONSE--------

Good point. We have added an appropriate footnote:

The Athena+ science theme “The hot and energetic Universe” was
recently selected for the second Large-class mission in ESA’s Cosmic
Vision science program.

---------------------

page 27: ALMA will also be good at high-resolution lens maps,
   potentially quite a bit better than JWST.

------RESPONSE--------

ALMA will have excellent resolution for images sufficiently 
bright in the mm/sub-mm. It is not clear that all quasar hosts fulfill this. 
We have changed the text to 

high resolution imaging of the lens system is obtained through space telescopes 
or future ground adaptive optics systems (radio/submillimeter telescopes such as ALMA may play a role as well).

----------------------

Typographical/stylistic:

General: 
 Replace errors and error bars with uncertainties throughout.
 There are a lot of case errors on Universe.
 K correction is not used consistently (K correction, $k$-correction, etc.)
 Chose one way to write microns.  'microns' '<number> um' and '<number>um'
   are all used.  If one of the latter, $\mu m$ would be preferred.


------RESPONSE--------

These now are regularized in the text.

----------------------


Page 2, line 21: 'detemine its velocity and acceleration'
       line 24: 'Plot distance as a function...' -> fragment
       line 27: akin to what?
       line 28: 'Or whether describing' -> not a sentence
       line 34: '<The> Dark Energy Survey'
       line 37: 'systematic problems' -> 'systematic uncertainties'
                problems and uncertainties are not synonymous.

------RESPONSE--------

The text is modified to eliminate the above problems.

----------------------

page 3, line 13: Projections from Planck?  That's a bit confusing, since
                Planck already released results, although they may mean 
		 projections for the completed Planck survey.  It is probably
		 bes to just leave it as 'input from CMB surveys' or
                somesuch.

------RESPONSE--------

We now use the referee-suggested "input from full Planck survey".

----------------------

       line 20: '...gravitational wave sirens have been identified
       as...'

------RESPONSE--------

Text corrected.

----------------------


page 4: 
       Figure 1: this caption is a disaster.
         'The top panel is a compilation of current measurements and
             projections for near-future projects.'
         'The Hubble diagram in comoving distance.  SN~Ia measure
             the luminosity distance, while...' 
         Please just give the equation relating the three measures inline
          rather than trying to describe it in words.
         'A current compilation of 580 SNe~Ia leads to...'
         '... Lya Forest BAO measurements provide evidence for the 
             existence...' 
         '...will conduce BAO measurements at various redshifts...'
         '...while the Prime Focus Spectrograh...'
         '...and the Hobby-Eberly...'
         'The expansion rate history'
         'H(z) / (1+z) (=adot) is the expansion rate...'  What is 
            'representative' about mathematical identity?
         'In a LCDM model...'
         '...the expansion of the Universe...'
         '...turns to acceleration...'
         '...when the fraction of dark energy in critical density...' 
            I can guess what the authors are trying to say, but this
            sentence isn't it.
         '...BAO probe the expansion rate...'
         No planned future set of experiments will trace the
          entire expansion history of the Universe (say, back to
          the Planck scale).  Remove entire.

        I'm not sure that entirely covers it.  Honestly, it might be better to
	 rewrite the caption from scratch than try to fix what is there now.


------RESPONSE--------

The caption has been completely overhauled to be more concise.

----------------------


page 5, line 8: '...means that enormous cosmic volmes are required to detect..'
       line 8: '..only recent generations of..' (not 'the recent generations)
       line 12: ''when combined with lower-redshift SDSS-II data this...'
       line 35: 'First, regarding precision.' -> Not a sentence.
       line 41: Reaching below 1% what?
       line 42: Missing . after possible.
       line 44: 'This limitation is called cosmic variance...'        
       line 47: It is not clear from the text that the authors mean
         figure 2 of -this- paper, which comes from [19].  It sounds like
         they mean the reader should go look up [19] and look for figure 2
	  in that paper.
       line 49: '...Fisher matrix forecasts from [39], and logarithmic...'
       line 54: '..there are diminishing returns in studying it at earlier...'
       line 57: 'The first detections...'

------RESPONSE--------

All have been fixed

-----

page 6, line 44: 'Next, regarding accuracy.' -> not a sentence.
    Figure 2: Replace Errors with uncertainties in caption and legend.

------RESPONSE--------

We fixed the first comment, but have left "errors" in the legend.

-----

page 7, figure 3: replace error with uncertainties in caption and legend.

------RESPONSE--------

as above, we have left "error" in the legend.

-----

page 8, line 10: '..sharply limits BAO measurements.'
       line 26: '...a new wide-field spectrograph on the Mayall 4-m
             telescope at Kitt Peak to...'
       line 32: 'a first percent-level look at BAO in the z...'

------RESPONSE--------

Fixed as suggested

-----

page 9, line 41: 'measures using the wrong cosmology' -> measures doesn't
              seem like the right word here.  Perhaps analyzed?

------RESPONSE--------

"measures" changed to "assumes"

-----


page 10, line 38: '...which will provide unprecedented...'
                 '......curves over a broad redshift range.'

------RESPONSE--------

We fix the first part but the second is as intended.

-------

page 11, line 24: '..the luminosity distance at the redshifts of interest
            is well approximated by'
        line 44: '..as a low-risk, high reward...'
        line 48: It may be the worst acronym of all time, but ESSENCE
                 needs to be capitalized.

------RESPONSE--------

The wording of line 44 is intentional.

-------


page 13, line 28: Extra ] after 'see Footnote blah'
page 14, line 15: ditto.
page 16, line 8: missing word after 'will allow'.  Exploitation?
page 17, line 33: '...the SN~Ia cosmology program could...'
page 18, line 11: 'In order to study...'
        line 16: '...redshifts supernovae are extremely faint, and...'
        line 22: '...Mission, WFIRST-2.4, uses a...'
        line 24: '...0.6 to 2.0 microns, and with...'
page 19, line 57: '...capable of constraining SN evolution...'

------RESPONSE--------

Measuring the expected SN evolution is our more ambitious goal.

-------


page 20, line 9: '...program, achieving such a capability may not be
                 likely in the upcoming decade.'  
page 21, figure 4: the measured spectrum of what? (the sky)
page 23, line 12: '...new pathways for SN cosmology in...'
        line 37: -may- have different, or -often- have different.
                 Some are the same, like GRB cosmology with SN cosmology.

------RESPONSE--------

The expectation is that all methods have systematic uncertainty and
that they are not all the same.

-------


page 24, line 26: '...simplifies the cosmogical analysis...'
page 25, line 47: '...measurements can be combined...'

page 31, line 8: '...do not substantially dim with time over human
                 timescales...' They may, after all, become quiescient
		  eventually.

------RESPONSE--------

Reworded as requested.

-------
