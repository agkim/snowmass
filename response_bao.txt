page 2, line 50: I question whether the DESI distance measurements
are fully independent.  Are the authors really stating that the 
off-diagonal covariance terms will be identically zero?  I would
remove 'independent' here.

-----
RESPONSE

The measurements are statistically independent.  As discussed in the BAO
text, the systematic errors are expected to be sub-dominant to the
statistical errors, even in the limit that the entire sky is observed.
We are therefore comfortable with the statement that the DESI distance
measurements are independent.

-----

page 3, line 51: While the detailed discussion does cover projects
 like HETDEX, and previous ones like wiggle-z, somehow the executive
 summary here reads as a rather biased take reflecting the projects
 that the people who were actually at Snomass are involved in.  I
 recommend mentioning HETDEX and PFS here.

 On a related note, is there some citation to back up the claim that
 the Lya forest is a lot more efficient than Lya emission galaxies?
 That's a strong enough statement it would be good if it were backed
 up a bit somewhere.

-----
RESPONSE

The first item is a good point, and we've decided to take the opposite
approach to that proposed by the referee.  Instead of mentioning surveys
by name, we simply state the following:
"Surveys in the coming decade will extend our view at $z>0.7$ by over
an order of magnitude in cosmic volume."

As for the second point, there are many ways to support the statement
that lya forest is more efficient that lya emission galaxies.  For one,
the BOSS BAO measurement has 1% precision in the CMASS sample and 2% precision
in the lya forest sample.  Both samples are tied to the same exposure sequence,
but we use almost a full order of magnitude larger sample of galaxies than
lya quasars (1.3M vs 175,000).  The lya emitting galaxies will be much less
efficient than the CMASS galaxies because they are typically a 10X fainter, so
they will be less efficient than lya forest as well.  Factoring in selection
efficiency will probably make the case even stronger for lya quasars.
For simplicity, we address this comment with a reference to the paper that
makes first projections for lya forest constraints, with some comparison
to galaxies.  In that paper, we find comparable constraints from 1000 galaxies
per sq degree as from 40 lya forest quasars per sq degree.  The reference to
McDonald and Eisenstein has been added.

-----

page 6, line 55: 'misweighting' is an odd choice. Rather than applying
  some value judgement, just say 'different weighting' or something.

  Figure 2: on the y axis, Fractional Uncertainty in what?
    I don't want to have to look at the caption to know.  Actually,
    the caption doesn't say either.
  Please note in the caption that the horizontal bars are bin sizes,
    not uncertainties.

-----
RESPONSE

'misweighting' changed to 'different weighting'

The fractional errors on are DA(z) and H(z), with different symbols for each.
The symbols are denoted on the bottom right hand side of the figure.
We have updated the caption to state this and clarified that the horizontal
bars are bin sizes and not uncertainties.

-----

page 7, figure 3:
  Please note in the caption that the horizontal bars are bin sizes,
    not uncertainties.
  What is the 1% H_0 value on the left?  Please explain in the caption,
    or remove it.

-----
RESPONSE

Bin sizes noted in caption.  We also added the following statement to the 
caption to clarify the confusion WRT 1% H0:
"The blue arrow shows the fractional error on that density that would
result from a hypothetical 1\% measurement of $H_0$."

-----

page 8, line 49: please provide a timeline for Euclid, and italicize
   if appropriate.  Also italicize WFIRST if appropriate for this journal.

-----
RESPONSE

We have added the following to the discussion of Euclid:
"The satellite is scheduled launch in 2020 with
six-year mission lifetime."

As for formatting, it is not clear what the journal requirements are, the
author guidelines simple state:  "There are no strict formatting requirements..."

-----

page 4: 
       line 57: 'The first detections...'

-----
RESPONSE

Fixed.
-----

page 5, line 8: '...means that enormous cosmic volmes are required to detect..'
       line 8: '..only recent generations of..' (not 'the recent generations)
       line 12: ''when combined with lower-redshift SDSS-II data this...'
       line 35: 'First, regarding precision.' -> Not a sentence.
       line 41: Reaching below 1% what?
       line 42: Missing . after possible.
       line 44: 'This limitation is called cosmic variance...'        
       line 47: It is not clear from the text that the authors mean
         figure 2 of -this- paper, which comes from [19].  It sounds like
         they mean the reader should go look up [19] and look for figure 2
	  in that paper.
       line 49: '...Fisher matrix forecasts from [39], and logarithmic...'
       line 54: '..there are diminishing returns in studying it at earlier...'

-----
RESPONSE

All have been fixed

-----

page 6, line 44: 'Next, regarding accuracy.' -> not a sentence.
    Figure 2: Replace Errors with uncertainties in caption and legend.

-----
RESPONSE

We fixed the first comment, but have left "errors" in the caption and legend.


-----

page 7, figure 3: replace error with uncertainties in caption and legend.

-----
RESPONSE

as above, we have left "error" in the caption and legend.

-----

page 8, line 10: '..sharply limits BAO measurements.'
       line 26: '...a new wide-field spectrograph on the Mayall 4-m
             telescope at Kitt Peak to...'
       line 32: 'a first percent-level look at BAO in the z...'

-----
RESPONSE

Fixed as suggested


-----

page 9, line 41: 'measures using the wrong cosmology' -> measures doesn't
              seem like the right word here.  Perhaps analyzed?

-----
RESPONSE

"measures" changed to "assumes"

-----

