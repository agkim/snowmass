\subsection{Active Galactic Nuclei Radius-Luminosity Relationship}

\subsubsection{Overview and current status} 
The most luminous Active Galactic Nuclei (AGNs) can be detected across much 
of the observable Universe, \textcolor{red}{and the spectral properties of 
most are remarkably uniform. These two properties have} motivated many 
attempts to find ways to use them as standard candles \cite{1977ApJ...214..679B,1999MNRAS.302L..24C,2002ApJ...581L..67E,wang13}.
One promising technique
that has been shown to yield a reliable luminosity distance indicator for AGN \cite{2011ApJ...740L..49W}
is to utilize the relationship between
the radius of the AGN broad line region (BLR) and 
the continuum luminosity \cite{2000ApJ...533..631K,2013ApJ...767..149B}. The BLR consists of 
high-velocity gas clouds that surround the supermassive black hole at the 
center of the AGN. These gas clouds are photoionized by radiation from 
the immediate vicinity of the black hole, and thus the distances at which 
these gas clouds produce emission in various lines are determined by the 
intensity of the ionizing continuum. The naive physical expectation is that 
the radius $R$ for a particular broad emission line should be proportional to 
the square root of the luminosity $L^{1/2}$, since the flux declines as the 
inverse square of the distance from the center. 

While the BLR is spatially unresolved, the radius of the BLR can be measured 
with the technique of reverberation mapping \cite{1982ApJ...255..419B,1993PASP..105..247P}. 
Reverberation mapping takes advantage of the intrinsic continuum variability 
of AGNs and that this continuum photoionizes the BLR. As the continuum varies 
in luminosity, the luminosities of the broad emission lines in the BLR also 
change, and the time lag $\tau$ between the continuum and line variations is 
due to the light travel time to the BLR. Measurement of this time lag 
provides the radius $R = c\tau$ of the BLR, and this in turn can be used to 
predict the intrinsic luminosity from the radius-luminosity relation. 

Time lags for $\sim 50$ moderate-luminosity AGNs with $z<0.3$ have been made to 
date (see \cite{2013ApJ...773...90G} for a recent reanalysis.) These lag measurements are 
typically based on continuum and spectroscopic observations on at least 30 
epochs, a cadence that is on order five times smaller than the lag or shorter, 
and few percent flux uncertainties in the line and continuum. With a careful 
consideration of all of the observational uncertainties, as well as accurate 
subtraction of the host galaxy starlight, the best fit for the
radius-luminosity relation has a power-law slope of $0.533^{+0.035}_{-0.033}$ 
\cite{2013ApJ...767..149B}. This result is based on AGNs that span four orders of 
magnitude in luminosity and is in good agreement with the expectation that the 
BLR is photoionized by the continuum. 

The intrinsic scatter in the radius-luminosity relationship for the best 
reverberation data is $\sim 0.11$ dex, which is not much larger than the 
typical uncertainty of $\sim 0.09$ dex in the data \cite{2010IAUS..267..151P}. 
The current uncertainties in an ``AGN Hubble Diagram'' for a larger number of 
AGNs have a root mean square scatter of 0.13 dex after two outliers are 
excluded \cite{2011ApJ...740L..49W,2013ApJ...767..149B}. Based on a reduced $\chi^2$ analysis, nearly 
half of the total scatter appears to be due to observational uncertainty. 
These estimates suggest that the scatter can be reduced to 0.08 dex or 
0.20 mag in distance modulus \cite{2011ApJ...740L..49W}. 

\subsubsection{Prospects for improvement}
One important, next step is to demonstrate that the uncertainties can be 
substantially reduced for a larger sample of nearby objects. The main 
requirement is more observations to produce better-sampled light curves, which 
would help eliminate incorrect lags due to aliasing or long-term variations. 
Other areas for improvement include reliable extinction corrections, better 
distance estimates for the nearest AGNs, more precise flux calibrations, and 
models to relate the variation in the observed continuum to the variation in 
the ionizing continuum. Many month-long campaigns at 1-m to 3-m class 
telescopes over the last few years \cite{2010ApJ...721..715D,2011ApJ...743L...4B} have gradually 
increased the number of AGNs with higher-quality observational data and begin 
to provide a dataset with which observational uncertainties can be minimized. 
A key advantage of AGNs is that since they do not substantially dim with time
\textcolor{red}{over human timescales}, 
these long-term studies can eliminate AGNs with uncertain extinction 
corrections, ambiguous lag measurements, and other potential complications. 

The greatest potential for the radius-luminosity relationship is the 
application of these same techniques to higher redshifts because the most 
luminous AGNs are several orders of magnitude brighter than the most luminous 
supernovae. The two main challenges to the application of this relationship 
to higher-redshift AGNs are: 1) The radius-luminosity relationship must be 
measured with different emission lines and calibrated for different continuum 
regions than have been used for low-redshift studies; 2) The most readily 
observable AGNs are the most luminous, and these also have the greatest lags 
(a problem that is compounded by time dilation). Low-redshift studies have 
concentrated on the prominent $H\beta$ emission line at 486~nm, yet by 
$z\sim0.6$ this line has redshifted out of the range of easy observation. 
There are numerous other rest-frame UV lines that are accessible in 
higher-redshift AGNs, but only a few have been studied with reverberation 
mapping \cite{2006ApJ...647..901M,2007ApJ...659..997K}, although microlensing studies of 
gravitationally-lensed QSOs do support the existence of a radius-luminosity 
relationship for the CIV broad emission line at 155~nm \cite{2013ApJ...764..160G}. Substantial, new data 
will be required to demonstrate that any of these other emission lines can 
ultimately lead to comparably small scatter\textcolor{red}{, and that 
evolutionary effects are either negligible or can be corrected}. 
Another consideration is that the 
radius-luminosity relationship implies that the time lags could be as long as 
several years for the most luminous objects, particularly those that would be 
easiest to detect at the highest redshifts, and thus observational campaigns 
will similarly require at least several years of data acquisition. Aliasing 
due to seasonal gaps in the data acquisition will also need to be addressed. 
One advantage is that the observing cadence is also slower, so less time is 
required in any one season. Another advantage is that the corrections for host 
galaxy starlight are less significant for the rest-frame UV continuum. A 
final, potentially unique advantage of AGNs is that the same methodology can be 
applied across the entire observable Universe. 

Substantial progress with high-redshift reverberation-mapping campaigns 
appears possible in the next few years thanks to both large, synoptic imaging 
surveys and multi-object spectrographs with sufficient field of view to 
observe many AGNs at once. Two current surveys that aim to study many new AGNs, 
including AGNs at higher redshifts, are DES and BOSS. DES aims to combine 
higher cadence photometric monitoring with lower cadence spectroscopy over the 
course of five years. These data will be used to estimate the radius-luminosity 
relationship for several emission lines in QSOs up to redshift four. BOSS plans 
to use slightly more spectroscopic epochs over a shorter period of time to 
accomplish similar aims, although directed more toward lower redshift and 
lower luminosity AGNs with shorter lag times. A key factor in the success of 
both surveys will be the quality of their data, particularly the relative flux 
calibration of spectroscopic data obtained through fiber optics cables. 
\textcolor{red}{If these surveys meet their data quality goals, the results
should begin to quantify the competitiveness of the radius-luminosity relation 
as a distance indicator at high redshift.} 

LSST offers unique and exciting opportunities to extend
reverberation\slash mapping 
studies at all redshifts and for a much broader range in luminosity. The LSST 
cadence is well suited to determine time lags for most AGNs of at least 
moderate luminosity, and the fraction of the sky that is nearly circumpolar 
offers an excellent opportunity to avoid aliasing. The combination of excellent 
flux calibration and multi-wavelength photometry, combined with the 
extraordinarily large sample size, also offers the prospect for 
reverberation-mapping with broad-band photometry alone
\cite{2013arXiv1310.6774Z}. Superb, 
multi-wavelength photometry, combined with a subset of AGNs with particularly 
strong emission lines, may lead to robust separation of the continuum and line 
intensity variations with photometric data alone. 

