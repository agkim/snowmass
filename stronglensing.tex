\subsection{Strong Lensing Time Delays}
Strong lensing time delay distances measure the geometric ratio of the 
distance between the observer and source, observer and deflecting lens, and 
lens and source, effectively the focal length of a gravitational lens: 
\begin{equation} 
D_{\Delta t}=(1+z_l)\frac{d_l d_s}{d_{ls}} \ , 
\end{equation} 
where the angular distances $d$ are functions of the matter density, dark 
energy density, and equation of state. 

This dimensionful quantity is observed through the time delays between 
the multiple images created of the source (typically a time varying quasar) 
by the lens (typically a galaxy).  By its nature, the time delay distance 
has two key powerful and unique properties: 1) Being dimensionful, it is 
directly sensitive to the Hubble constant $H_0$, and 2) Being a distance 
ratio, it has different covariances between cosmological parameters than 
luminosity (e.g.\ supernovae) or angular (e.g.\ BAO) distances, and is highly 
complementary for cosmological constraints on dark energy \cite{lin11}. 

Moreover, the strong lensing probe does not require an elaborate 
independent observing program, for the most part building on planned wide 
field and time domain surveys, and so is highly cost effective.  The 
lensed, multiply imaged quasars are detected in wide field surveys, 
then followed up in time domain surveys over a period of several years. 
Redshifts of the images and lenses are measured through spectroscopy, in a 
non time critical way, and 
\textcolor{red}{high resolution imaging of the lens system is obtained through space telescopes 
or future ground adaptive optics systems (radio/submillimeter telescopes such as 
ALMA may play a role as well).}
Out of the thousands 
of strong lens systems expected to be measured, we are free to choose the 
cleanest for distance measurements. 

As a new geometric method of exploring dark energy, strong lensing time 
delay distances are a valuable addition to 
standard techniques.  Already the method has matured to the level where 
single lensing systems have delivered 6\% distance measurements (including 
systematics), and programs are underway to increase the quantity and quality 
of measurements.  

In this decade, wide field imaging surveys such as Dark Energy Survey should 
discover $\sim 10^3$ lensed quasars, enabling the detailed study of 
$\sim$100 of 
these systems and resulting in substantial gains in the dark energy figure 
of merit.  For example this would increase the FOM from supernovae 
in combination (hence from purely geometric probes) by a factor of almost 5, 
due to 
degeneracy breaking.  In the next decade, a further order of magnitude 
improvement will be possible with the $10^4$ systems expected to be detected 
and measured with LSST and Euclid. To fully exploit these gains, three 
priorities are to: 
\begin{itemize} 
\item Support the development of techniques required for the accurate 
analysis of the data. 
\item Transform small robotic telescopes (1-4m in diameter) into a high 
cadence, long term dark energy monitoring experiment (non-exclusive use) 
to keep up with the discoveries. 
\item Support high resolution imaging capabilities, such as those 
enabled by the James Webb Space Telescope and next generation adaptive 
optics systems on large ground based telescopes. 
\end{itemize} 

Current blind analyses of time delay distances \cite{suyu10,suyu13a,komatsu11} 
demonstrate the maturing power of strong lensing as a dark energy probe.  
Figure~\ref{fig:cosmolens} illustrates the current cosmological leverage 
from a mere two time delay distances (left panel) and the maturity of 
modeling the gravitational lens (center and right panels).  
Moreover, the observed time delays can give signatures of microlensing from 
dark matter substructure along the line of sight, making this method a 
probe of dark matter properties as well.  The Snowmass white paper 
{\it Dark Energy with Gravitational Time Delays\/} \cite{snowsl} gives 
greater detail on this new dark energy probe. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{figure}[!h]
\includegraphics[width=0.4\textwidth]{SL2wmap7.pdf} 
  \includegraphics[trim=0 0 100 520, clip,width=0.59\textwidth]{RXJrecon.pdf}
  \caption{[Left panel] 68\% and 95\% confidence limits on the dark energy 
equation of state $w$ and the curvature $\Omega_k$ for WMAP7 combined with 
just two time delay distances, compared to other probes, showing the high 
complementarity of strong lensing.  
Observed HST [center panel] and reconstructed 
[right panel] image of a strong lens.  The lens model reproduces to high 
  fidelity tens of thousands of data points providing extremely tight 
  constraints on the mass model of the deflector and thus on 
  cosmological parameters.  Figures from \cite{suyu13a,suyu13b}. 
} 
\label{fig:cosmolens} 
\end{figure}

\textcolor{red}{
The main challenge currently for strong lensing as a cosmological probe 
is the small sample size; this will be greatly ameliorated by DES and LSST, 
and in that era the scarcity will be in follow up with high resolution 
imaging. On the systematics side, once we move below 5\% precision then 
accurately accounting for the projected mass, e.g.\ through using large 
ray tracing simulations to calibrate the convergence for a field with the 
observed galaxy density, will require careful attention. 
}